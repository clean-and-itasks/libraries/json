definition module Text.Unicode.Encodings.UTF8.GenJSON

from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Text.Unicode.Encodings.UTF8 import :: UTF8

derive JSONEncode UTF8
derive JSONDecode UTF8
