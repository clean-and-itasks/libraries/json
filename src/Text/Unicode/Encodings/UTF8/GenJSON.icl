implementation module Text.Unicode.Encodings.UTF8.GenJSON

import StdEnv
import Text.GenJSON
from Text.Unicode.Encodings.UTF8 import :: UTF8

derive JSONEncode UTF8
derive JSONDecode UTF8
