# Changelog

#### 3.1.1

- Chore: Accept text ^2.

### 3.1.0

- Feature: add `JSONEncode` and `JSONDecode` derivations for `:: UTF8`.

## 3.0.0

- Removed: Undo the changes made in version 2 of this package.
           The generic instances for `FileError`, `Map`, `Set` and `MaybeError`
		   are now defined in system/containers depending on where
		   the respective type is defined.

### 2.1.0

- Feature: add json derivations for `:: FileError`.

#### 2.0.1

- Fix: specify containers dependency to avoid breaking other packages
       which use this package but do not depend on containers.

## 2.0.0

- Feature: Add derives for Map, Set, IntMap and MaybeError.

### 1.1.0

- Add derives for `Either` type

#### 1.0.1

- Drop `Use` dependency on `text` and `containers`

## 1.0.0

- Initial version, import modules from clean platform v0.3.36 and destill all
  json modules.
